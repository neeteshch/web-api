﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("gettxns"),Authorize]
        public ActionResult<IEnumerable<Txns>> GetTxns()
        {
            return new Txns[] { new Txns { Id = 1,senderBranch= "AL0020",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 2,senderBranch= "AL0020",senderName= "g",txnStatus= "Escrow" },
            new Txns { Id = 3,senderBranch= "GFGFGFG",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 4,senderBranch= "ALTRTR0020",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 5,senderBranch= "TRTRTRT",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 6,senderBranch= "AL0GFG020",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 7,senderBranch= "GFGFGG",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 1,senderBranch= "AL0020",senderName= "Celica",txnStatus= "Paymen Done" },
            new Txns { Id = 8,senderBranch= "FGFGF",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 9,senderBranch= "SXCVVVV",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 10,senderBranch= "DSDSD",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 11,senderBranch= "LOIYHH",senderName= "Celica",txnStatus= "Escrow" },
            new Txns { Id = 12,senderBranch= "BNMJHG",senderName= "Celica",txnStatus= "Pass" }};
        }

        // GET api/values/5
        [HttpGet("id")]
        public ActionResult<string> Get(int id, int age)
        {
            return "value";
        }

        // POST api/values
        [HttpPost("dologin")]
        public ActionResult Post([FromBody] Login login)
        {
            return new JsonResult("Success");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }

    public class Txns
    {
        public int Id { get; set; }
        public string senderName { get; set; }
        public string senderBranch { get; set; }
        public string txnStatus { get; set; }
    }

    public class Login
    {
        public string loginname { get; set; }
        public string userpassword { get; set; }
    }
}
